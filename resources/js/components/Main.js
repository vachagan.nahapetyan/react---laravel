import React, { Component } from 'react';
import Home from './Home';
import Country from './Country';
import { Route, Link } from 'react-router-dom';

import Courses from './Courses';


export default class Main extends Component
{

    render(){
        return (
            <div>
                <ul>
                    <li><Link to="/home">Home</Link></li>
                    <li><Link to="/country">Country</Link></li>
                    <li><Link to="/courses">Courses</Link></li>
                </ul>

                <Route exact path="/home" component={Home}/>
                <Route exact path="/country" component={Country}/>
                <Route path="/courses" component={Courses}/>

            </div>
            
        )
    }


}