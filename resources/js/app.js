

require('./bootstrap');
import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import Main from './components/Main';
import ReactDOM from 'react-dom';




ReactDOM.render(
    <BrowserRouter>
        <Main />
    </BrowserRouter>, document.getElementById('example'));

